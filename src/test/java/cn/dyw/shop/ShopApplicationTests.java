package cn.dyw.shop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class ShopApplicationTests {

//    @Test
    public void contextLoads() throws IOException {

        String[] con = new String[]{"Category", "Comment", "Goods", "Merchant", "Order", "ShoppingCart", "User"};

        String tmp = "package cn.dyw.shop.mapping;\n" +
                "\n" +
                "import cn.dyw.shop.model.{};\n" +
                "import cn.dyw.shop.service.{}Service;\n" +
                "import org.springframework.beans.factory.annotation.Autowired;\n" +
                "import org.springframework.stereotype.Controller;\n" +
                "import org.springframework.web.bind.annotation.RequestMapping;\n" +
                "\n" +
                "/**\n" +
                " * @author: dyw770\n" +
                " * @date: 2018/5/6\n" +
                " */\n" +
                "@Controller\n" +
                "@RequestMapping(\"/{}\")\n" +
                "public class {}Mapping extends BaseMapping<{}> {\n" +
                "\n" +
                "    @Autowired\n" +
                "    protected void setBaseService({}Service {}Service) {\n" +
                "        this.baseService = {}Service;\n" +
                "    }\n" +
                "}\n";
        for (int i = 0; i < con.length; i++) {
            StringBuilder sb = new StringBuilder(tmp);
            File file = new File(con[i] + "Mapping.java");
            file.createNewFile();
            for (int j = 0; j < 8; j++) {
                if (j == 0 || j == 1 || j == 3 || j == 4 || j == 5) {
                    sb.replace(sb.indexOf("{}"), sb.indexOf("{}") + 2, con[i]);
                } else {
                    sb.replace(sb.indexOf("{}"), sb.indexOf("{}") + 2, StringUtils.uncapitalize(con[i]));
                }
            }
            OutputStream outputStream = new FileOutputStream(file);
            outputStream.write(sb.toString().getBytes("utf-8"));
            outputStream.flush();
            outputStream.close();
        }
    }

}
