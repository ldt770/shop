package cn.dyw.shop;

import com.mysql.jdbc.Driver;
import org.junit.Test;

import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author: dyw770
 * @date: 2018/5/10
 */
public class DriveTest {

    @Test
    public void testDrive() throws Exception {
        URL url = new URL("http://info.haocai138.com/jsData/matchResult/2017-2018/s203_1776.js");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        Scanner scanner = new Scanner(connection.getInputStream(), "UTF-8");
        while (scanner.hasNextLine()) {
            System.out.println(scanner.nextLine());
        }
    }
}
