package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.model.Address;
import cn.dyw.shop.model.User;
import cn.dyw.shop.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/address")
public class AddressMapping extends BaseMapping<Address> {

    @Autowired
    protected void setBaseService(AddressService addressService) {
        this.baseService = addressService;
    }

    @ResponseBody
    @RequestMapping("/find/user")
    public Json findAll(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return Json.factory(((AddressService) baseService).findUserAddress(user.getId()), true, 1001, "success");
    }

    @ResponseBody
    @RequestMapping("/add")
    public Json add(HttpSession session, Address address) {
        User user = (User) session.getAttribute("user");
        address.setUser(user);
        baseService.save(address);
        return Json.factory(address, true, 1001, "success");
    }
}
