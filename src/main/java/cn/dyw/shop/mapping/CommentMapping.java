package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.model.Comment;
import cn.dyw.shop.service.CommentService;
import cn.dyw.shop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/comment")
public class CommentMapping extends BaseMapping<Comment> {

    @Autowired
    private OrderService orderService;

    @Autowired
    protected void setBaseService(CommentService commentService) {
        this.baseService = commentService;
    }

    @ResponseBody
    @GetMapping("/find/comment")
    public Json findComment(@RequestParam("id") Integer id) {
        return Json.factory(((CommentService) baseService).findComment(id), true, 1001, "success");
    }

    @ResponseBody
    @GetMapping("/reply")
    public Json reply(@RequestParam("id") Integer id, @RequestParam("reply") String reply) {
        Comment one = baseService.findOne(id);
        one.setReply(reply);
        baseService.save(one);
        return Json.factory(((CommentService) baseService).findComment(id), true, 1001, "success");
    }
}
