package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.model.*;
import cn.dyw.shop.service.GoodsService;
import cn.dyw.shop.service.OrderItemService;
import cn.dyw.shop.service.OrderService;
import cn.dyw.shop.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/order")
public class OrderMapping extends BaseMapping<Order> {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    protected void setBaseService(OrderService orderService) {
        this.baseService = orderService;
    }

    /**
     * 生成订单
     * @param session
     * @param order
     * @return
     */
    @ResponseBody
    @RequestMapping("/add")
    public Json addOrder(HttpSession session, Order order) {
        User user = (User) session.getAttribute("user");
        order.setUser(user);
        order.setPay(false);
        baseService.save(order);
        for (OrderItem item : order.getOrderItems()) {
            item.setComment(false);
            item.setOrder(order);
        }
        orderItemService.save(order.getOrderItems());
        return Json.factory(order, true, 1001, "success");
    }

    /**
     * 支付
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/pay")
    public Json pay(@RequestParam("id") Integer id) {
        Order one = baseService.getOne(id);
        ((OrderService) baseService).pay(one);
        return Json.factory(one, true, 1001, "success");
    }

    /**
     * 申请退款
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/refund")
    public Json refund(@RequestParam("id") Integer id) {
        OrderItem one = orderItemService.findOne(id);
        one.setRefund(1);
        orderItemService.save(one);
        return Json.factory(one, true, 1001, "申请退款成功");
    }

    /**
     * 处理退款 2是拒绝 3 是通过
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/refund/op")
    public Json refundOp(@RequestParam("id") Integer id, @RequestParam("op") Integer op) {
        OrderItem one = orderItemService.findOne(id);
        one.setRefund(op);
        orderItemService.save(one);
        return Json.factory(one, true, 1001, "操作成功");
    }

    /**
     * 查询退款
     * @return
     */
    @ResponseBody
    @RequestMapping("/refund/all")
    public Json refundAll() {
        return Json.factory(orderItemService.refund(), true, 1001, "success");
    }

    /**
     * 所有的订单
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/find/all")
    public Json findAll(HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Order> orders = ((OrderService) baseService).findAllByUserId(user.getId());
        return Json.factory(orders, true, 1001, "success");
    }

    @ResponseBody
    @RequestMapping("/find/item")
    public Json findItem(HttpSession session) {
        Merchant user = (Merchant) session.getAttribute("user");
        List<OrderItem> sh = orderItemService.sh(user.getId());
        return Json.factory(sh, true, 1001, "success");
    }
}
