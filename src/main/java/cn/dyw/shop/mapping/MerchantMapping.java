package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.model.Merchant;
import cn.dyw.shop.service.GoodsService;
import cn.dyw.shop.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/merchant")
public class MerchantMapping extends BaseMapping<Merchant> {

    @Autowired
    protected void setBaseService(MerchantService merchantService) {
        this.baseService = merchantService;
    }

    @Autowired
    private GoodsService goodsService;

    /**
     * 登录
     * @param merchant
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/login")
    public Json login(Merchant merchant, HttpSession session) {
        Merchant login = ((MerchantService) baseService).login(merchant);
        if (login != null) {
            session.setAttribute("user", login);
            return Json.factory(login, true, 1001, "success");
        } else {
            return Json.factory(null, false, 1002, "fal");
        }
    }

    /**
     * 注册
     * @param merchant
     * @return
     */
    @ResponseBody
    @RequestMapping("/register")
    public Json register(Merchant merchant, MultipartFile file) {
        merchant.setHead(file(file));
        baseService.save(merchant);
        return Json.factory(merchant, true, 1001, "success");
    }

    /**
     * 资料
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/data")
    public Json register(HttpSession session) {
        Merchant merchant = (Merchant) session.getAttribute("user");
        if (merchant == null) {
            return Json.factory(merchant, false, 1002, "请先登录");
        }
        return Json.factory(merchant, true, 1001, "success");
    }

    @ResponseBody
    @GetMapping("/find/m/goods")
    public Json findMGoods(HttpSession session) {
        Merchant user = (Merchant) session.getAttribute("user");
        return Json.factory(goodsService.findMGoods(user.getId()), true, 1001, "success");
    }

    public String file(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        File file1 = new File(  "H:/study/idea/shop/resources/upload/" + fileName);
        if (!file1.getParentFile().exists()) {
            file1.mkdirs();
        }
        if (file1.exists()) {
            try {
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            file.transferTo(file1);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return "/resources/upload/" + fileName;
    }
}
