package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.service.imp.BaseService;
import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/4/18
 */
public class BaseMapping<T> {

    protected BaseService<T> baseService;

    @ResponseBody
    @GetMapping("/find/id")
    public Json findById(@RequestParam("id") Integer id) {
        T t = baseService.findOne(id);
        return Json.factory(t, true, 1001, "sucess");
    }

    @ResponseBody
    @GetMapping("/find/all")
    public Json findAll() {
        List<T> t = baseService.findAll();
        return Json.factory(t, true, 1001, "sucess");
    }

    @ResponseBody
    @PostMapping("/updata")
    public Json update(T t) {
        baseService.save(t);
        return Json.factory(t, true, 1001, "sucess");
    }

    @ResponseBody
    @PostMapping("/delete")
    public Json deleteById(Integer id) {
        baseService.delete(id);
        return Json.factory(id, true, 1001, "sucess");
    }

    @ResponseBody
    @PostMapping("/save")
    public Json save(T t) {
        baseService.save(t);
        return Json.factory(t, true, 1001, "sucess");
    }
}
