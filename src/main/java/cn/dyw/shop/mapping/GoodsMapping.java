package cn.dyw.shop.mapping;

import cn.dyw.shop.json.Json;
import cn.dyw.shop.model.Goods;
import cn.dyw.shop.model.Merchant;
import cn.dyw.shop.model.User;
import cn.dyw.shop.service.GoodsService;
import cn.dyw.shop.utils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/goods")
public class GoodsMapping extends BaseMapping<Goods> {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    protected void setBaseService(GoodsService goodsService) {
        this.baseService = goodsService;
    }

    @ResponseBody
    @GetMapping("/find/category/{id}")
    public Json findByCategoryId(@PathVariable("id") Integer id) {
        return Json.factory(goodsService.findByCategoryId(id),  true, 1001, "success");
    }

    @ResponseBody
    @GetMapping("/find/name")
    public Json findByCategoryId(@RequestParam("name") String name) {
        return Json.factory(goodsService.findByName(name),  true, 1001, "success");
    }

    @ResponseBody
    @GetMapping("/find/del")
    public Json finDel() {
        return Json.factory(goodsService.findByDel(false), true, 1001, "success");
    }

    @ResponseBody
    @PostMapping("/save2")
    public Json save(HttpSession session, Goods goods, MultipartFile file) {
        goods.setPrc(file(file));
        Merchant user = (Merchant) session.getAttribute("user");
        goods.setMerchant(user);
        goods.setDel(false);
        goods.setSalesVolume(0);
        baseService.save(goods);
        return Json.factory(goods, true, 1001, "success");
    }

    @ResponseBody
    @PostMapping("/update2")
    public Json update(Goods goods, MultipartFile file) {
        goods.setPrc(file(file));
        Goods one = baseService.findOne(goods.getId());
        BeanUtils.copyNotNullProperties(goods, one);
        baseService.save(one);
        return Json.factory(one, true, 1001, "success");
    }

    @ResponseBody
    @GetMapping("/tuijain")
    public Json tuijain(HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return Json.factory(goodsService.reMenSP(), true, 1001, "success");
        } else {
            return Json.factory(goodsService.reMenSP(), true, 1001, "success");
        }
    }

    public String file(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        File file1 = new File(  "H:/study/idea/shop/resources/upload/" + fileName);
        if (!file1.getParentFile().exists()) {
            file1.mkdirs();
        }
        if (file1.exists()) {
            try {
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            file.transferTo(file1);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return "/resources/upload/" + fileName;
    }
}
