package cn.dyw.shop.mapping;

import cn.dyw.shop.model.Category;
import cn.dyw.shop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: dyw770
 * @date: 2018/5/6
 */
@Controller
@RequestMapping("/category")
public class CategoryMapping extends BaseMapping<Category> {

    @Autowired
    protected void setBaseService(CategoryService categoryService) {
        this.baseService = categoryService;
    }
}
