package cn.dyw.shop.json;

/**
 * @author dyw770
 * @date 2017/11/04 02:09:14
 *
 * 消息通信模板
 */
public class Json {
	
	/**
	 * 是否成功
	 */
	private boolean success;
	
	/**
	 * 标识
	 */
	private int code;

	/**
	 * 返回对象
	 */
	private Object obj;
	
	/**
	 * 消息
	 */
	private String msg;
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	/**
	 * 工厂构建
	 * @param obj
	 * @return
	 */
	public static Json factory(Object obj, boolean b, int code, String msg) {
		Json json = new Json();
		json.setObj(obj);
		json.setSuccess(b);
		json.setCode(code);
		json.setMsg(msg);
		return json;
	}
}
