package cn.dyw.shop.model;
// Generated 2018-5-12 12:17:09 by Hibernate Tools 5.1.0.Alpha1

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Comment generated by hbm2java
 */
@JsonIgnoreProperties(value={"handler", "hibernateLazyInitializer", "fieldHandler"})
@Entity
@Table(name = "comment", catalog = "shop")
public class Comment implements java.io.Serializable {

	private Integer id;
	private OrderItem orderItem;
	private String context;
	private String reply;

	public Comment() {
	}

	public Comment(OrderItem orderItem) {
		this.orderItem = orderItem;
	}

	public Comment(OrderItem orderItem, String context, String reply) {
		this.orderItem = orderItem;
		this.context = context;
		this.reply = reply;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_item_id", nullable = false)
	public OrderItem getOrderItem() {
		return this.orderItem;
	}

	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}

	@Column(name = "context", length = 45)
	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	@Column(name = "reply", length = 45)
	public String getReply() {
		return this.reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

}
