package cn.dyw.shop.dao;

import cn.dyw.shop.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface CategoryDao extends JpaRepository<Category, Integer> {
}
