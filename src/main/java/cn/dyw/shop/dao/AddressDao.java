package cn.dyw.shop.dao;

import cn.dyw.shop.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface AddressDao extends JpaRepository<Address, Integer> {

    List<Address> findAllByUserId(Integer id);
}
