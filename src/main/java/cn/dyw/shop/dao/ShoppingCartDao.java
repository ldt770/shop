package cn.dyw.shop.dao;

import cn.dyw.shop.model.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface ShoppingCartDao extends JpaRepository<ShoppingCart, Integer> {

    @Query("update ShoppingCart s set s.num = ?2 where s.id = ?1")
    ShoppingCart updateCart(Integer id, Integer num);

    void deleteByUserId(Integer uid);

    List<ShoppingCart> findAllByUserId(Integer id);
}
