package cn.dyw.shop.dao;

import cn.dyw.shop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface UserDao extends JpaRepository<User, Integer> {

    @Query("from User u where u.code = :code and u.pwd = :pwd")
    User login(@Param("pwd") String pwd, @Param("code") String code);

}
