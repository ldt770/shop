package cn.dyw.shop.dao;

import cn.dyw.shop.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentDao extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByOrderItemGoodsId(Integer id);
}
