package cn.dyw.shop.dao;

import cn.dyw.shop.model.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface GoodsDao extends JpaRepository<Goods, Integer> {


    List<Goods> findByCategoryId(Integer id);

    List<Goods> findByDel(Boolean del);

    List<Goods> findAllByNameLike(String name);

    List<Goods> findByMerchantIdAndDelIs(Integer id, boolean del);

    List<Goods> findAllByOrderBySalesVolumeDesc();
}
