package cn.dyw.shop.dao;

import cn.dyw.shop.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface OrderDao extends JpaRepository<Order, Integer> {

    List<Order> findAllByUserId(Integer id);
}
