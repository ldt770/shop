package cn.dyw.shop.dao;

import cn.dyw.shop.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/12
 */
public interface OrderItemDao extends JpaRepository<OrderItem, Integer> {

    List<OrderItem> findByRefundIsNot(Integer refund);

    @Query("from OrderItem o where o.goods.merchant.id = :id")
    List<OrderItem> findItem(@Param("id") Integer id);
}
