package cn.dyw.shop.dao;

import cn.dyw.shop.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
public interface MerchantDao extends JpaRepository<Merchant, Integer> {

    @Query("from  Merchant m  where m.code = :code and m.pwd = :pwd")
    Merchant login(@Param("code") String code, @Param("pwd") String pwd);
}
