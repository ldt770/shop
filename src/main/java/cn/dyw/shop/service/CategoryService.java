package cn.dyw.shop.service;

import cn.dyw.shop.dao.CategoryDao;
import cn.dyw.shop.model.Category;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class CategoryService extends BaseService<Category> {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    public void setAddressDao(CategoryDao categoryDao) {
        this.repository = categoryDao;
    }
}
