package cn.dyw.shop.service;

import cn.dyw.shop.dao.MerchantDao;
import cn.dyw.shop.model.Merchant;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class MerchantService extends BaseService<Merchant> {

    @Autowired
    private MerchantDao merchantDao;

    @Autowired
    public void setAddressDao(MerchantDao merchantDao) {
        this.repository = merchantDao;
    }

    public Merchant login(Merchant merchant) {
        return merchantDao.login(merchant.getCode(), merchant.getPwd());
    }
}