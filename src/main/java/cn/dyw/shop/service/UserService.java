package cn.dyw.shop.service;

import cn.dyw.shop.dao.UserDao;
import cn.dyw.shop.model.User;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class UserService extends BaseService<User> {

    @Autowired
    private UserDao userDao;

    @Autowired
    public void setAddressDao(UserDao userDao) {
        this.repository = userDao;
    }

    public User login(User user) {
        return userDao.login(user.getPwd(), user.getCode());
    }
}
