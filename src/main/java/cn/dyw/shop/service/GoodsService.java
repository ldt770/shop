package cn.dyw.shop.service;

import cn.dyw.shop.dao.GoodsDao;
import cn.dyw.shop.model.Goods;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class GoodsService extends BaseService<Goods> {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    public void setAddressDao(GoodsDao goodsDao) {
        this.repository = goodsDao;
    }

    public List<Goods> findByCategoryId(Integer id) {
        return goodsDao.findByCategoryId(id);
    }

    public List<Goods> findByDel(Boolean del) {
        return goodsDao.findByDel(del);
    }

    public List<Goods> findByName(String name) {
        name = "%" + name + "%";
        return goodsDao.findAllByNameLike(name);
    }

    public List<Goods> findMGoods(Integer id) {
        return goodsDao.findByMerchantIdAndDelIs(id, false);
    }

    public List<Goods> reMenSP() {
        List<Goods> goodsList = new ArrayList<>();
        List<Goods> goods = goodsDao.findAllByOrderBySalesVolumeDesc();
        if (goods.size() > 5) {
            for (int i = 0; i < 5; i++) {
                goodsList.add(goods.get(i));
            }
            return goodsList;
        } else {
            return goods;
        }
    }
}
