package cn.dyw.shop.service;

import cn.dyw.shop.dao.CommentDao;
import cn.dyw.shop.model.Comment;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class CommentService extends BaseService<Comment> {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    public void setAddressDao(CommentDao commentDao) {
        this.repository = commentDao;
    }

    public List<Comment> findComment(Integer id) {
        return commentDao.findAllByOrderItemGoodsId(id);
    }
}
