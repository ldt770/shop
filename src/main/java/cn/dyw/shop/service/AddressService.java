package cn.dyw.shop.service;

import cn.dyw.shop.dao.AddressDao;
import cn.dyw.shop.model.Address;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class AddressService extends BaseService<Address> {

    @Autowired
    private AddressDao addressDao;

    @Autowired
    public void setAddressDao(AddressDao addressDao) {
        this.repository = addressDao;
    }

    public List<Address> findUserAddress(Integer id) {
        return addressDao.findAllByUserId(id);
    }
}
