package cn.dyw.shop.service.imp;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public class BaseService<T> {

    protected JpaRepository<T, Integer> repository;

    public List<T> findAll() {
        return repository.findAll();
    }

    public List<T> findAll(Sort sort) {
        return repository.findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public List<T> findAll(Iterable<Integer> iterable) {
        return repository.findAll(iterable);
    }

    public long count() {
        return repository.count();
    }

    public void delete(Integer integer) {
        repository.delete(integer);
    }

    public void delete(T t) {
        repository.delete(t);
    }

    public void delete(Iterable<? extends T> iterable) {
        repository.delete(iterable);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public <S extends T> S save(S s) {
        return repository.save(s);
    }

    public <S extends T> List<S> save(Iterable<S> iterable) {
        return repository.save(iterable);
    }

    public T findOne(Integer integer) {
        return repository.findOne(integer);
    }

    public boolean exists(Integer integer) {
        return repository.exists(integer);
    }

    public void flush() {
        repository.flush();
    }

    public <S extends T> S saveAndFlush(S s) {
        return repository.saveAndFlush(s);
    }

    public void deleteInBatch(Iterable<T> iterable) {
        repository.deleteInBatch(iterable);
    }

    public void deleteAllInBatch() {
        repository.deleteAllInBatch();
    }

    public T getOne(Integer integer) {
        return repository.getOne(integer);
    }

    public <S extends T> S findOne(Example<S> example) {
        return repository.findOne(example);
    }

    public <S extends T> List<S> findAll(Example<S> example) {
        return repository.findAll(example);
    }

    public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
        return repository.findAll(example, sort);
    }

    public <S extends T> Page<S> findAll(Example<S> example, Pageable pageable) {
        return repository.findAll(example, pageable);
    }

    public <S extends T> long count(Example<S> example) {
        return repository.count(example);
    }

    public <S extends T> boolean exists(Example<S> example) {
        return repository.exists(example);
    }
}
