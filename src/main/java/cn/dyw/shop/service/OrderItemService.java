package cn.dyw.shop.service;

import cn.dyw.shop.dao.OrderItemDao;
import cn.dyw.shop.model.OrderItem;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/12
 */
@Service
public class OrderItemService extends BaseService<OrderItem> {

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    public void setAddressDao(OrderItemDao orderItemDao) {
        this.repository = orderItemDao;
    }

    public List<OrderItem> refund() {
        return orderItemDao.findByRefundIsNot(0);
    }

    public List<OrderItem> sh(Integer id) {
        return orderItemDao.findItem(id);
    }
}
