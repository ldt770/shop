package cn.dyw.shop.service;

import cn.dyw.shop.dao.ShoppingCartDao;
import cn.dyw.shop.model.ShoppingCart;
import cn.dyw.shop.service.imp.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: dyw770
 * @date: 2018/5/5
 */
@Service
public class ShoppingCartService extends BaseService<ShoppingCart> {

    @Autowired
    private ShoppingCartDao shoppingCartDao;

    @Autowired
    public void setAddressDao(ShoppingCartDao shoppingCartDao) {
        this.repository = shoppingCartDao;
    }

    public ShoppingCart updateCart(Integer id, Integer num) {
        return shoppingCartDao.updateCart(id, num);
    }

    public void deleteAll(Integer uid) {
        shoppingCartDao.deleteByUserId(uid);
    }

    public List<ShoppingCart> findByUserId(Integer id) {
        return shoppingCartDao.findAllByUserId(id);
    }
}
