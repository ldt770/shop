// 获取url值
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var reg_rewrite = new RegExp("(^|/)" + name + "/([^/]*)(/|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    var q = window.location.pathname.substr(1).match(reg_rewrite);
    if(r != null){
        return unescape(r[2]);
    }else if(q != null){
        return unescape(q[2]);
    }else{
        return null;
    }
}

function openpage(url){
    if(validate()){
        window.open(url);
    }else{
        window.location.href="login";
    }
}

function validate() {
    // $.ajax({
    // 	url: '/path/to/file',
    // 	type: 'default GET (Other values: POST)',
    // 	dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    // 	data: {param1: 'value1'},
    // })
    // .done(function() {
    // 	console.log("success");
    // })
    // .fail(function() {
    // 	console.log("error");
    // })
    // .always(function() {
    // 	console.log("complete");
    // });

    if(localStorage.getItem("shopuser")){
    	return true;
    }else {
    	return false;
    }
    
}

function gotosingle(id) {
    localStorage.goodsid=id;
    window.location.href="single";
}

function  addcart(id,num) {
    $.ajax({
    	url: '/shoppingCart/add',
    	type: 'POST',
    	data: {
    	    id: id,
            num:num
        },
    })
    .done(function(data) {
    	console.log("success");
        console.log(data)
        if (data.code==1001){
            alert("添加成功")
        } else{
            alert("添加失败")
        }
    })
    .fail(function() {
    	console.log("error");
    })
    .always(function() {
    	console.log("complete");
    });
}